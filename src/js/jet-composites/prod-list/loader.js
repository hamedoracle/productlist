/**
  Copyright (c) 2015, 2018, Oracle and/or its affiliates.
  The Universal Permissive License (UPL), Version 1.0
*/
define(['ojs/ojcomposite', 'text!./prod-list-view.html', './prod-list-viewModel.js', 'text!./component.json', 'css!./prod-list-styles'],
  function(Composite, view, viewModel, metadata) {
    Composite.register('prod-list', {
      view: view,
      viewModel: viewModel,
      metadata: JSON.parse(metadata)
    });
  }
);
