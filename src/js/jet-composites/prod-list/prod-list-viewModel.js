/**
  Copyright (c) 2015, 2018, Oracle and/or its affiliates.
  The Universal Permissive License (UPL), Version 1.0
*/
'use strict';
define(
    ['knockout'
      , 'jquery'
      , 'ojL10n!./resources/nls/prod-list-strings.js'
      , 'ojs/ojknockout'
      , 'ojs/ojlistview'
      , 'ojs/ojmodel'
      , 'ojs/ojgauge'
      , 'ojs/ojbutton'
      , 'ojs/ojcheckboxset'
      , 'ojs/ojselectcombobox'
      , 'ojs/ojpagingcontrol'
      , 'ojs/ojcollectiontabledatasource'
      , 'ojs/ojpagingtabledatasource'], function (ko, $, componentStrings) {

    function ExampleComponentModel(context) {
        var self = this;
        self.addCardListener = function(event,ui) {
        var params = {
          'bubbles': true,
          'detail': {'value': ui.data.ID}
        };
        self.composite.dispatchEvent(new CustomEvent('addCardListener', params));
        }
        //At the start of your viewModel constructor
        var busyContext = oj.Context.getContext(context.element).getBusyContext();
        var options = {"description": "CCA Startup - Waiting for data"};
        self.busyResolve = busyContext.addBusyState(options);

        self.composite = context.element;

        //Example observable
        self.messageText = ko.observable('Hello from Example Component');
        self.properties = context.properties;
        self.res = componentStrings['prod-list'];

        var criteriaMap = {};
        criteriaMap['lh'] = {
          key: 'PRICE',
          direction: 'ascending'
        };
        criteriaMap['hl'] = {
          key: 'PRICE',
          direction: 'descending'
        };
        criteriaMap['reviews'] = {
          key: 'REVIEWS',
          direction: 'descending'
        };
        criteriaMap['date'] = {
          key: 'PUBLISH_DATE',
          direction: 'ascending'
        };

        var filters = ['lt30', '30to40', '40to50', 'gt50', 'five', 'four', 'three', 'two', 'dcoward', 'jbrock', 'hschildt', 'jmanico', 'mnaftalin'];

        var filterFunc = {};
        filterFunc['lt30'] = function(model) {
          return (parseFloat(model.get('PRICE')) < 30);
        };
        filterFunc['30to40'] = function(model) {
          return (parseFloat(model.get('PRICE')) > 30 && parseFloat(model.get('PRICE')) < 40);
        };
        filterFunc['40to50'] = function(model) {
          return (parseFloat(model.get('PRICE')) >= 40 && parseFloat(model.get('PRICE')) <= 50);
        };
        filterFunc['gt50'] = function(model) {
          return (parseFloat(model.get('PRICE')) > 50);
        };

        filterFunc['five'] = function(model) {
          return (parseFloat(model.get('RATING')) == 5);
        };
        filterFunc['four'] = function(model) {
          return (parseFloat(model.get('RATING')) >= 4);
        };
        filterFunc['three'] = function(model) {
          return (parseFloat(model.get('RATING')) >= 3);
        };
        filterFunc['two'] = function(model) {
          return (parseFloat(model.get('RATING')) < 3);
        };

        filterFunc['dcoward'] = function(model) {
          return (model.get('AUTHOR').indexOf('Danny Coward') > -1);
        };
        filterFunc['jbrock'] = function(model) {
          return (model.get('AUTHOR').indexOf('John Brock') > -1);
        };
        filterFunc['jmanico'] = function(model) {
          return (model.get('AUTHOR').indexOf('Jim Manico') > -1);
        };
        filterFunc['hschildt'] = function(model) {
          return (model.get('AUTHOR').indexOf('Herbert Schildt') > -1);
        };
        filterFunc['mnaftalin'] = function(model) {
          return (model.get('AUTHOR').indexOf('Maurice Naftalin') > -1);
        };

        var converterFactory = oj.Validation.converterFactory("number");
        var currencyOptions = {
          style: "currency",
          currency: "USD",
          currencyDisplay: "symbol"
        };
        self.currencyConverter = converterFactory.createConverter(currencyOptions);

        var model = oj.Model.extend({
          idAttribute: 'ID'
        });

        var url = window.location.href;
        var arr = url.split("/");
        var result = arr[0] + "//" + arr[2];

        self.collection = new oj.Collection(null, {
          url: result + '/js/data/product.json',
          model: model
        });
        var originalCollection = self.collection;

        self.dataSource = ko.observable(new oj.PagingTableDataSource(new oj.CollectionTableDataSource(self.collection)));

        self.currentPrice = [];
        self.currentAuthor = [];
        self.currentRating = [];
        self.currentSort = ko.observable("default");

        self.handleSortCriteriaChanged = function(event, ui) {
          var criteria = criteriaMap[event.detail.value];
          self.dataSource().sort(criteria);
        };

        self.handleFilterChanged = function(event, ui) {
          var value = event.detail.value;
          if (!Array.isArray(value)) {
            return;
          }

          var results = [];
          var processed = false;

          $.each(filters, function(index, filter) {
            if (value.indexOf(filter) > -1) {
              results = results.concat(originalCollection.filter(filterFunc[filter]));
              processed = true;
            }
          });

          if (processed) {
            self.collection = new oj.Collection(results);
          } else {
            self.collection = originalCollection;
          }
          self.dataSource(new oj.PagingTableDataSource(new oj.CollectionTableDataSource(self.collection)));

          if (self.currentSort() != "default") {
            var criteria = criteriaMap[self.currentSort()];
            self.dataSource().sort(criteria);
          }

        // Example for parsing context properties
        // if (context.properties.name) {
        //     parse the context properties here
        // }

        //Once all startup and async activities have finished, relocate if there are any async activities
        self.busyResolve();
    };
  }

    //Lifecycle methods - uncomment and implement if necessary
    //ExampleComponentModel.prototype.activated = function(context){
    //};

    //ExampleComponentModel.prototype.connected = function(context){
    //};

    //ExampleComponentModel.prototype.bindingsApplied = function(context){
    //};

    //ExampleComponentModel.prototype.disconnect = function(context){
    //};

    //ExampleComponentModel.prototype.propertyChanged = function(context){
    //};

    return ExampleComponentModel;
});
